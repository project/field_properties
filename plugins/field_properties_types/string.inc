<?php

/**
 * @file
 * 'string' type plugin for metadata field
 */

$plugin = array(
  'name' => 'string',
  'title' => 'String',
  'weight' => -50,
);
